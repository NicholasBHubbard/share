#!/bin/bash

CXX=${CXX:-g++}
CXX_FLAGS=""
BUILD_DIR=build
SRC_DIR=src

if [[ ! -d $BUILD_DIR ]]; then
    mkdir $BUILD_DIR
fi

# Compile the source files
$CXX $CXX_FLAGS -o build/megamu $SRC_DIR/main.cpp $SRC_DIR/mmap.cpp $SRC_DIR/m68k/m68kcpu.c $SRC_DIR/m68k/m68kops.c $SRC_DIR/m68k/m68kdasm.c $SRC_DIR/m68k/softfloat/softfloat.c

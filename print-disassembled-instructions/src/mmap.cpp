#include <stdint.h>
#include <stdio.h>
#include "mmap.h"
#include "m68k/m68k.h"

// the memory map
static MMap mmap = MMap {};

MMap *the_mmap()
{
	return &mmap;
}

// definitions for Musashi ...

unsigned int m68k_read_memory_8(unsigned int addr)
{
	addr &= 0xFFFFFF;

	uint8_t byte;

	// ROM
	if (addr < 0x400000)
		byte = mmap.rom[addr];

	// RAM
	if (addr >= 0xE00000)
		byte = mmap.ram[addr-0xE00000];

	return static_cast<unsigned int>(byte);
}

unsigned int m68k_read_memory_16(unsigned int addr)
{
	// TODO
	// if((addr & 0xe700e0) == 0xc00000)
	//	return ReadVDPW(addr&0x1e);

	unsigned int high_byte = m68k_read_memory_8(addr);
	unsigned int low_byte = m68k_read_memory_8(addr+1);

	unsigned int word = (high_byte << 8) | low_byte;

	return word;
}

unsigned int m68k_read_memory_32(unsigned int addr)
{
	unsigned int high_word = m68k_read_memory_16(addr);
	unsigned int low_word = m68k_read_memory_16(addr+2);

	unsigned int long_word = (high_word << 16) | low_word;

	return long_word;
}

void m68k_write_memory_8(unsigned int addr, unsigned int val)
{
	addr &= 0xFFFFFF;

	// ROM is read-only
	// if (addr < 0x400000)
	// 	m_rom[addr] = val;

	// RAM
	if (addr >= 0xE00000)
		mmap.ram[addr-0xE00000] = static_cast<uint8_t>(val);
}

void m68k_write_memory_16(unsigned int addr, unsigned int val)
{
	unsigned int high_byte = (val & 0xFF00) >> 8;
	unsigned int low_byte = val & 0xFF;

	m68k_write_memory_8(addr, high_byte);
	m68k_write_memory_8(addr+1, low_byte);
}

void m68k_write_memory_32(unsigned int addr, unsigned int val)
{
	unsigned int high_word = (val & 0xFFFF0000) >> 16;
	unsigned int low_word = val & 0xFFFF;

	m68k_write_memory_16(addr, high_word);
	m68k_write_memory_16(addr+2, low_word);
}

unsigned int m68k_read_disassembler_8(unsigned int addr) {
	return m68k_read_memory_8(addr);
}

unsigned int m68k_read_disassembler_16 (unsigned int addr) {
	return m68k_read_memory_16(addr);
}

unsigned int m68k_read_disassembler_32 (unsigned int addr) {
	return m68k_read_memory_32(addr);
}

void print_disassembled_instruction(unsigned int pc)
{
	char buf[100];
	m68k_disassemble(buf, pc, M68K_CPU_TYPE_68000);
	printf("(pc $%06x): %s\n", pc & 0xFFFFFF, buf);
	fflush(stdout);
}

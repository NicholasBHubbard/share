#include <string>
#include <fstream>
#include "m68k/m68k.h"
#include "mmap.h"

// takes single arg for path to ROM
int main(int argc, char *argv[])
{
	MMap *mmap = the_mmap();

	// load the ROM
	std::string rom_path = argv[1];
	std::ifstream rom_ifs(rom_path, std::ios::binary|std::ios::ate);
	int rom_size = rom_ifs.tellg();
	rom_ifs.seekg(0);
	rom_ifs.read((char*)mmap->rom, rom_size);
	rom_ifs.close();

	// initialize the cpu
	m68k_init();
	m68k_set_cpu_type(M68K_CPU_TYPE_68000);
	m68k_pulse_reset();

	printf("starting execution after initializing the cpu ...\n");

	while (1) {
		m68k_execute(10000);
	}
}

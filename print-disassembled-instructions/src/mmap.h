// the memory map

// https://segaretro.org/Sega_Mega_Drive/Memory_map

#include <stdint.h>
#include <string>
#include "m68k/m68k.h"

typedef struct MMap {
	uint8_t rom[0x400000];
	uint8_t ram[0xFFFFFF-0xE00000];
} MMap;

MMap *the_mmap();

void my_disassemble_instruction(unsigned int pc);

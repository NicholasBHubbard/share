I pushed my branch `allow-user-specified-ruby-version`, which isn't completely
tested yet, but can be used to install specific Ruby versions with
Alien::Ruby (by setting the ALIEN_RUBY_VERSION environment variable).

The test code given for determining the `$LOAD_PATH` works on Ruby 1.X and 3.X versions, however, it fails
on any Ruby 2.X version. The reason why is that there are built-in libraries that are always
loaded when Ruby is invoked. These libraries are `rubygems.rb` and `rbconfig.rb`. This means that the
given test code has a chicken+egg problem, where it needs to talk to Ruby in order to set `RUBYLIB`,
however we cannot talk to Ruby without first setting `RUBYLIB`.

Here is the error we get from running `ruby -e 'puts "hello"'`, on a Ruby 2.X
during test time:

```
<internal:gem_prelude>:1:in `require': cannot load such file -- rubygems.rb (LoadError)
        from <internal:gem_prelude>:1:in `<compiled>'

```

It is important to note that Ruby 3.X has similar behavior, but it only gives a warning when it
cannot find these default libraries instead of dying like Ruby 2.X, so we are still able
to successfully run the test program that finds the value of `$LOAD_PATH`.

According to the [Ruby manual](https://ruby-doc.org/core-2.5.0/doc/globals_rdoc.html)
the `$LOADED_FEATURES` variable contains all the module names loaded by require. The
following shows the value of `$LOADED_FEATURES` on a fresh Ruby 2.7.7 that is
installed by Alien::Ruby (by skipping the tests):

```
$ perl -e 'use Alien::Ruby; use Env qw(@PATH); unshift @PATH, Alien::Ruby->bin_dir; system(q(ruby), q(-e), q($LOADED_FEATURES.each {|v| puts v}))'
enumerator.so
thread.rb
rational.so
complex.so
ruby2_keywords.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/x86_64-linux/enc/encdb.so
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/x86_64-linux/enc/trans/transdb.so
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/x86_64-linux/rbconfig.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rubygems/compatibility.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rubygems/defaults.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rubygems/deprecate.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rubygems/errors.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rubygems/version.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rubygems/requirement.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rubygems/platform.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rubygems/basic_specification.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rubygems/stub_specification.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rubygems/util.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rubygems/text.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rubygems/user_interaction.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rubygems/specification_policy.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rubygems/util/list.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rubygems/specification.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rubygems/exceptions.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rubygems/bundler_version_finder.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rubygems/dependency.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rubygems/core_ext/kernel_gem.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/x86_64-linux/monitor.so
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/monitor.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rubygems/core_ext/kernel_require.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rubygems/core_ext/kernel_warn.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rubygems.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rubygems/path_support.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/did_you_mean/version.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/did_you_mean/core_ext/name_error.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/did_you_mean/levenshtein.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/did_you_mean/jaro_winkler.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/did_you_mean/spell_checker.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/did_you_mean/spell_checkers/name_error_checkers/class_name_checker.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/did_you_mean/spell_checkers/name_error_checkers/variable_name_checker.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/did_you_mean/spell_checkers/name_error_checkers.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/did_you_mean/spell_checkers/method_name_checker.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/did_you_mean/spell_checkers/key_error_checker.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/did_you_mean/spell_checkers/null_checker.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/did_you_mean/formatters/plain_formatter.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/did_you_mean/tree_spell_checker.rb
/usr/local/lib/x86_64-linux-gnu/perl/5.32.1/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/did_you_mean.rb
```

This output shows that some built-in libraries are loaded by default even when
we write a program without any explicit calls to `require`.

I was able to fix the problem on Linux for Ruby version's 2.7.X by replacing the
proposed test with the following code.

```
@RUBYLIB = (
    Alien::Ruby->dist_dir . '/lib/ruby/2.7.0',
    Alien::Ruby->dist_dir . '/lib/ruby/2.7.0/x86_64-linux'
);
```

This fixes the problem because all of the default built-in libraries that Ruby
needs in order to run and load the `English` library are in the two directories
that I added to `@RUBYLIB`.

Unfortunately this solution has a couple of important problems. First it is
hardcoded to only work for Ruby 2.7.X's. Also note that for the version number
is 2.7.0 even though it works for any 2.7.X. The more difficult problem of this
code is that one of the libraries is named after the OS and architecture (Linux
x86_64). This means this solution will not work on Windows, BSD, 32-bit systems,
etc. If for example we are on FreeBSD13 then all instance of `x86_64-linux` are
replaced with `amd64-freebsd13`, and on Windows (RubyInstaller) we get
`x64-mingw-ucrt`. It seems that this value comes from Ruby's
[RUBY_PLATFORM](https://ruby-doc.org/docs/ruby-doc-bundle/Manual/man-1.4/variable.html#platform)
constant.

To avoid having to hardcode in version numbers and OS/arch strings into `@RUBYLIB`,
we could instead use something like [File::Find](https://perldoc.perl.org/File::Find) to search `Alien::Ruby->dist_dir . '/lib/ruby'` to find directories that should be added to `@RUBYLIB`. I came up
with the following code:

```
use File::Find;
use File::Spec;

my $ruby_lib_root = Alien::Ruby->dist_dir . '/lib/ruby';

find(
    sub {
        if (-d $File::Find::name) {
            my $lib = $File::Find::name =~ s/^$ruby_lib_root//r;
            my $depth = File::Spec->splitdir($lib) - 1;
            if ($depth <= 2) {
                push @RUBYLIB, $ruby_lib_root . $lib;
            }
        }
    },
    $ruby_lib_root
);
```

This adds all directories at depths less or equal to 2 from `$ruby_lib_root` to
`@RUBYLIB`.  This inlcudes the paths neccesary for us to be able to test that we
can load and use a built-in library. However, the downside of this is that
unnecessary paths are added to `@RUBYLIB`. The following shows the state of
`@RUBYLIB` after running this code (Ruby 2.7.7):

```
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/x86_64-linux
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/bigdecimal
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/digest
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/fiddle
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/io
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/json
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/openssl
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/psych
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/ripper
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/forwardable
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/syslog
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/benchmark
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/bundler
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/cgi
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/csv
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/delegate
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/did_you_mean
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/drb
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/getoptlong
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/irb
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/logger
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/matrix
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/net
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/observer
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/open3
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/optparse
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/ostruct
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/pstore
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/racc
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rdoc
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/reline
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rexml
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rinda
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rss
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/rubygems
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/singleton
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/timeout
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/tracer
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/unicode_normalize
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/uri
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/webrick
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/2.7.0/yaml
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/gems
/root/.cpanm/work/1676749679.7/Alien-Ruby-0.01/blib/lib/auto/share/dist/Alien-Ruby/lib/ruby/gems/2.7.0
```

Let me know your thoughts!

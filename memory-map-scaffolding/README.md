This code has some basic scaffolding that should allow me to start implementing some vdp/z80 code.

One thing I am concerned about is how accessing 16-bit control ports (such as the vdp control port) in two 8-bit read/writes will work. This code only actually reads a byte at a time (via m68k_read_memory_8 and m68k_write_memory_8), as the functions for accessing 16-bits and 32-bits are just wrappers around the 8-bit accesses. I suspect this may cause me some problems with accessing 16-bit control ports as these ports are not just raw memory, but rather will be used as input/output to some other function in the program. Is this concern justified?

This output is from the ROM built with the code from here (from the article you linked me): https://github.com/namalgo/genesis/blob/main/src/very_minimal/very_minimal.asm. The output ends up going on forever printing these `dbra D1, $292` instructions, however there is quite a bit of output before that, and the program doesn't crash with an `UNIMPLEMENTED MEMORY READ/WRITE`.

```
$ ./build.sh
$ build/megamu roms/test-rom.md
(pc $000204) move    #$2700, SR                  (opc $46fc2700)
(pc $000208) move.b  $a10001.l, D0               (opc $103900a10001)
VERSION REGISTER READ: mem $a10001
(pc $00020e) andi.b  #$f, D0                     (opc $0200000f)
(pc $000212) beq     $21e                        (opc $670a)
(pc $00021e) move.w  #$100, $a11100.l            (opc $33fc010000a11100)
Z80 BUS REQUEST WRITE: val $1 to mem $a11100
Z80 BUS REQUEST WRITE: val $0 to mem $a11101
(pc $000226) move.w  #$100, $a11200.l            (opc $33fc010000a11200)
Z80 RESET WRITE: val $1 to mem $a11200
Z80 RESET WRITE: val $0 to mem $a11201
(pc $00022e) btst    #$0, $a11101.l              (opc $0839000000a11101)
Z80 BUS REQUEST READ: mem $a11101
(pc $000236) bne     $22e                        (opc $66f6)
(pc $000238) movea.l #$a00000, A1                (opc $227c00a00000)
(pc $00023e) move.l  #$c30000, (A1)              (opc $22bc00c30000)
Z80 MEMORY WRITE: val $0 to mem $a00000
Z80 MEMORY WRITE: val $c3 to mem $a00001
Z80 MEMORY WRITE: val $0 to mem $a00002
Z80 MEMORY WRITE: val $0 to mem $a00003
(pc $000244) move.w  #$0, $a11200.l              (opc $33fc000000a11200)
Z80 RESET WRITE: val $0 to mem $a11200
Z80 RESET WRITE: val $0 to mem $a11201
(pc $00024c) move.w  #$0, $a11100.l              (opc $33fc000000a11100)
Z80 BUS REQUEST WRITE: val $0 to mem $a11100
Z80 BUS REQUEST WRITE: val $0 to mem $a11101
(pc $000254) lea     ($46,PC), A0; ($29c)        (opc $41fa0046)
(pc $000258) moveq   #$18, D0                    (opc $7018)
(pc $00025a) move.l  #$8000, D1                  (opc $223c00008000)
(pc $000260) move.b  (A0)+, D1                   (opc $1218)
(pc $000262) move.w  D1, $c00004.l               (opc $33c100c00004)
VDP CONTROL PORT WRITE: val $80 to mem $c00004
VDP CONTROL PORT WRITE: val $5 to mem $c00005
(pc $000268) add.w   #$100, D1                   (opc $d27c0100)
(pc $00026c) dbra    D0, $260                    (opc $51c8fff2)
(pc $000260) move.b  (A0)+, D1                   (opc $1218)
(pc $000262) move.w  D1, $c00004.l               (opc $33c100c00004)
VDP CONTROL PORT WRITE: val $81 to mem $c00004
VDP CONTROL PORT WRITE: val $44 to mem $c00005
(pc $000268) add.w   #$100, D1                   (opc $d27c0100)
(pc $00026c) dbra    D0, $260                    (opc $51c8fff2)
(pc $000260) move.b  (A0)+, D1                   (opc $1218)
(pc $000262) move.w  D1, $c00004.l               (opc $33c100c00004)
VDP CONTROL PORT WRITE: val $82 to mem $c00004
VDP CONTROL PORT WRITE: val $38 to mem $c00005
(pc $000268) add.w   #$100, D1                   (opc $d27c0100)
(pc $00026c) dbra    D0, $260                    (opc $51c8fff2)
(pc $000260) move.b  (A0)+, D1                   (opc $1218)
(pc $000262) move.w  D1, $c00004.l               (opc $33c100c00004)
VDP CONTROL PORT WRITE: val $83 to mem $c00004
VDP CONTROL PORT WRITE: val $0 to mem $c00005
(pc $000268) add.w   #$100, D1                   (opc $d27c0100)
(pc $00026c) dbra    D0, $260                    (opc $51c8fff2)
(pc $000260) move.b  (A0)+, D1                   (opc $1218)
(pc $000262) move.w  D1, $c00004.l               (opc $33c100c00004)
VDP CONTROL PORT WRITE: val $84 to mem $c00004
VDP CONTROL PORT WRITE: val $7 to mem $c00005
(pc $000268) add.w   #$100, D1                   (opc $d27c0100)
(pc $00026c) dbra    D0, $260                    (opc $51c8fff2)
(pc $000260) move.b  (A0)+, D1                   (opc $1218)
(pc $000262) move.w  D1, $c00004.l               (opc $33c100c00004)
VDP CONTROL PORT WRITE: val $85 to mem $c00004
VDP CONTROL PORT WRITE: val $78 to mem $c00005
(pc $000268) add.w   #$100, D1                   (opc $d27c0100)
(pc $00026c) dbra    D0, $260                    (opc $51c8fff2)
(pc $000260) move.b  (A0)+, D1                   (opc $1218)
(pc $000262) move.w  D1, $c00004.l               (opc $33c100c00004)
VDP CONTROL PORT WRITE: val $86 to mem $c00004
VDP CONTROL PORT WRITE: val $0 to mem $c00005
(pc $000268) add.w   #$100, D1                   (opc $d27c0100)
(pc $00026c) dbra    D0, $260                    (opc $51c8fff2)
(pc $000260) move.b  (A0)+, D1                   (opc $1218)
(pc $000262) move.w  D1, $c00004.l               (opc $33c100c00004)
VDP CONTROL PORT WRITE: val $87 to mem $c00004
VDP CONTROL PORT WRITE: val $0 to mem $c00005
(pc $000268) add.w   #$100, D1                   (opc $d27c0100)
(pc $00026c) dbra    D0, $260                    (opc $51c8fff2)
(pc $000260) move.b  (A0)+, D1                   (opc $1218)
(pc $000262) move.w  D1, $c00004.l               (opc $33c100c00004)
VDP CONTROL PORT WRITE: val $88 to mem $c00004
VDP CONTROL PORT WRITE: val $0 to mem $c00005
(pc $000268) add.w   #$100, D1                   (opc $d27c0100)
(pc $00026c) dbra    D0, $260                    (opc $51c8fff2)
(pc $000260) move.b  (A0)+, D1                   (opc $1218)
(pc $000262) move.w  D1, $c00004.l               (opc $33c100c00004)
VDP CONTROL PORT WRITE: val $89 to mem $c00004
VDP CONTROL PORT WRITE: val $0 to mem $c00005
(pc $000268) add.w   #$100, D1                   (opc $d27c0100)
(pc $00026c) dbra    D0, $260                    (opc $51c8fff2)
(pc $000260) move.b  (A0)+, D1                   (opc $1218)
(pc $000262) move.w  D1, $c00004.l               (opc $33c100c00004)
VDP CONTROL PORT WRITE: val $8a to mem $c00004
VDP CONTROL PORT WRITE: val $0 to mem $c00005
(pc $000268) add.w   #$100, D1                   (opc $d27c0100)
(pc $00026c) dbra    D0, $260                    (opc $51c8fff2)
(pc $000260) move.b  (A0)+, D1                   (opc $1218)
(pc $000262) move.w  D1, $c00004.l               (opc $33c100c00004)
VDP CONTROL PORT WRITE: val $8b to mem $c00004
VDP CONTROL PORT WRITE: val $0 to mem $c00005
(pc $000268) add.w   #$100, D1                   (opc $d27c0100)
(pc $00026c) dbra    D0, $260                    (opc $51c8fff2)
(pc $000260) move.b  (A0)+, D1                   (opc $1218)
(pc $000262) move.w  D1, $c00004.l               (opc $33c100c00004)
VDP CONTROL PORT WRITE: val $8c to mem $c00004
VDP CONTROL PORT WRITE: val $0 to mem $c00005
(pc $000268) add.w   #$100, D1                   (opc $d27c0100)
(pc $00026c) dbra    D0, $260                    (opc $51c8fff2)
(pc $000260) move.b  (A0)+, D1                   (opc $1218)
(pc $000262) move.w  D1, $c00004.l               (opc $33c100c00004)
VDP CONTROL PORT WRITE: val $8d to mem $c00004
VDP CONTROL PORT WRITE: val $3f to mem $c00005
(pc $000268) add.w   #$100, D1                   (opc $d27c0100)
(pc $00026c) dbra    D0, $260                    (opc $51c8fff2)
(pc $000260) move.b  (A0)+, D1                   (opc $1218)
(pc $000262) move.w  D1, $c00004.l               (opc $33c100c00004)
VDP CONTROL PORT WRITE: val $8e to mem $c00004
VDP CONTROL PORT WRITE: val $0 to mem $c00005
(pc $000268) add.w   #$100, D1                   (opc $d27c0100)
(pc $00026c) dbra    D0, $260                    (opc $51c8fff2)
(pc $000260) move.b  (A0)+, D1                   (opc $1218)
(pc $000262) move.w  D1, $c00004.l               (opc $33c100c00004)
VDP CONTROL PORT WRITE: val $8f to mem $c00004
VDP CONTROL PORT WRITE: val $2 to mem $c00005
(pc $000268) add.w   #$100, D1                   (opc $d27c0100)
(pc $00026c) dbra    D0, $260                    (opc $51c8fff2)
(pc $000260) move.b  (A0)+, D1                   (opc $1218)
(pc $000262) move.w  D1, $c00004.l               (opc $33c100c00004)
VDP CONTROL PORT WRITE: val $90 to mem $c00004
VDP CONTROL PORT WRITE: val $0 to mem $c00005
(pc $000268) add.w   #$100, D1                   (opc $d27c0100)
(pc $00026c) dbra    D0, $260                    (opc $51c8fff2)
(pc $000260) move.b  (A0)+, D1                   (opc $1218)
(pc $000262) move.w  D1, $c00004.l               (opc $33c100c00004)
VDP CONTROL PORT WRITE: val $91 to mem $c00004
VDP CONTROL PORT WRITE: val $0 to mem $c00005
(pc $000268) add.w   #$100, D1                   (opc $d27c0100)
(pc $00026c) dbra    D0, $260                    (opc $51c8fff2)
(pc $000260) move.b  (A0)+, D1                   (opc $1218)
(pc $000262) move.w  D1, $c00004.l               (opc $33c100c00004)
VDP CONTROL PORT WRITE: val $92 to mem $c00004
VDP CONTROL PORT WRITE: val $0 to mem $c00005
(pc $000268) add.w   #$100, D1                   (opc $d27c0100)
(pc $00026c) dbra    D0, $260                    (opc $51c8fff2)
(pc $000260) move.b  (A0)+, D1                   (opc $1218)
(pc $000262) move.w  D1, $c00004.l               (opc $33c100c00004)
VDP CONTROL PORT WRITE: val $93 to mem $c00004
VDP CONTROL PORT WRITE: val $0 to mem $c00005
(pc $000268) add.w   #$100, D1                   (opc $d27c0100)
(pc $00026c) dbra    D0, $260                    (opc $51c8fff2)
(pc $000260) move.b  (A0)+, D1                   (opc $1218)
(pc $000262) move.w  D1, $c00004.l               (opc $33c100c00004)
VDP CONTROL PORT WRITE: val $94 to mem $c00004
VDP CONTROL PORT WRITE: val $0 to mem $c00005
(pc $000268) add.w   #$100, D1                   (opc $d27c0100)
(pc $00026c) dbra    D0, $260                    (opc $51c8fff2)
(pc $000260) move.b  (A0)+, D1                   (opc $1218)
(pc $000262) move.w  D1, $c00004.l               (opc $33c100c00004)
VDP CONTROL PORT WRITE: val $95 to mem $c00004
VDP CONTROL PORT WRITE: val $0 to mem $c00005
(pc $000268) add.w   #$100, D1                   (opc $d27c0100)
(pc $00026c) dbra    D0, $260                    (opc $51c8fff2)
(pc $000260) move.b  (A0)+, D1                   (opc $1218)
(pc $000262) move.w  D1, $c00004.l               (opc $33c100c00004)
VDP CONTROL PORT WRITE: val $96 to mem $c00004
VDP CONTROL PORT WRITE: val $0 to mem $c00005
(pc $000268) add.w   #$100, D1                   (opc $d27c0100)
(pc $00026c) dbra    D0, $260                    (opc $51c8fff2)
(pc $000260) move.b  (A0)+, D1                   (opc $1218)
(pc $000262) move.w  D1, $c00004.l               (opc $33c100c00004)
VDP CONTROL PORT WRITE: val $97 to mem $c00004
VDP CONTROL PORT WRITE: val $80 to mem $c00005
(pc $000268) add.w   #$100, D1                   (opc $d27c0100)
(pc $00026c) dbra    D0, $260                    (opc $51c8fff2)
(pc $000260) move.b  (A0)+, D1                   (opc $1218)
(pc $000262) move.w  D1, $c00004.l               (opc $33c100c00004)
VDP CONTROL PORT WRITE: val $98 to mem $c00004
VDP CONTROL PORT WRITE: val $0 to mem $c00005
(pc $000268) add.w   #$100, D1                   (opc $d27c0100)
(pc $00026c) dbra    D0, $260                    (opc $51c8fff2)
(pc $000270) move.w  #$0, D0                     (opc $303c0000)
(pc $000274) move.w  #$8f00, $c00004.l           (opc $33fc8f0000c00004)
VDP CONTROL PORT WRITE: val $8f to mem $c00004
VDP CONTROL PORT WRITE: val $0 to mem $c00005
(pc $00027c) move.l  #$c0000003, $c00004.l       (opc $23fcc000000300c00004)
VDP CONTROL PORT WRITE: val $c0 to mem $c00004
VDP CONTROL PORT WRITE: val $0 to mem $c00005
VDP CONTROL PORT WRITE: val $0 to mem $c00006
VDP CONTROL PORT WRITE: val $3 to mem $c00007
(pc $000286) move.w  D0, $c00000.l               (opc $33c000c00000)
VDP DATA PORT WRITE: val $0 to mem $c00000
VDP DATA PORT WRITE: val $0 to mem $c00001
(pc $00028c) addq.w  #1, D0                      (opc $5240)
(pc $00028e) move.w  #$64, D1                    (opc $323c0064)
(pc $000292) dbra    D1, $292                    (opc $51c9fffe)
(pc $000292) dbra    D1, $292                    (opc $51c9fffe)
(pc $000292) dbra    D1, $292                    (opc $51c9fffe)
(pc $000292) dbra    D1, $292                    (opc $51c9fffe)
(pc $000292) dbra    D1, $292                    (opc $51c9fffe)
(pc $000292) dbra    D1, $292                    (opc $51c9fffe)
(pc $000292) dbra    D1, $292                    (opc $51c9fffe)
...
```

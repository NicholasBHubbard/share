#include <string>
#include <fstream>
#include <stdint.h>
#include "m68k/m68k.h"
#include "mmap.h"

// TODO: figure out where to put this
void print_disassembled_instruction(unsigned int pc)
{
	char buf[100];
	int instr_size = m68k_disassemble(buf, pc, M68K_CPU_TYPE_68000);
	printf("(pc $%06x) %-35s ", pc & 0xFFFFFF, buf);
	printf("(opc $", buf);
	for (int i = 0; i < instr_size; i++)
	{
		printf("%02x", m68k_read_memory_8(pc+i));
	}
	printf(")\n", buf);
	fflush(stdout);
}

// takes single arg for path to ROM
int main(int argc, char *argv[])
{
	MMap *mmap = the_mmap();

	// load the ROM
	std::string rom_path = argv[1];
	std::ifstream rom_ifs(rom_path, std::ios::binary|std::ios::ate);
	int rom_size = rom_ifs.tellg();
	rom_ifs.seekg(0);
	rom_ifs.read((char*)mmap->rom, rom_size);
	rom_ifs.close();

	// initialize the cpu
	m68k_init();
	m68k_set_cpu_type(M68K_CPU_TYPE_68000);
	m68k_pulse_reset();

	// main loop
	while (1) {
		m68k_execute(100000);
	}
}

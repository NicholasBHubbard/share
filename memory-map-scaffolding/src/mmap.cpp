#include <stdint.h>
#include <stdio.h>
#include "mmap.h"
#include "m68k/m68k.h"

// the memory map
static MMap mmap = MMap {};
MMap *the_mmap()
{
	return &mmap;
}

unsigned int m68k_read_memory_8(unsigned int addr)
{
	addr &= 0xFFFFFF;

	uint8_t byte = 0;
	
	if (addr >= ROM_START && addr <= ROM_END)
	{
		byte = mmap.rom[addr-ROM_START];
	}
	else if (addr >= RAM_START && addr <= RAM_END)
	{
		byte = mmap.ram[addr-RAM_START];
	}
	else if (addr >= Z80_MEMORY_START && addr <= Z80_MEMORY_END)
	{
		fprintf(stderr, "Z80 MEMORY READ: mem $%x\n", addr);
	}
	else if ((addr >= VDP_DATA_PORT_START && addr <= VDP_DATA_PORT_END)
		  || (addr >= VDP_DATA_PORT_MIRROR_START && addr <= VDP_DATA_PORT_MIRROR_END))
	{
		fprintf(stderr, "VDP DATA PORT READ: mem $%x\n", addr);
	}
	else if ((addr >= VDP_CONTROL_PORT_START && addr <= VDP_CONTROL_PORT_END)
		  || (addr >= VDP_CONTROL_PORT_MIRROR_START && addr <= VDP_CONTROL_PORT_MIRROR_END))
	{
		fprintf(stderr, "VDP_CONTROL_PORT READ: mem $%x\n", addr);
	}
	else if (addr >= Z80_BUS_REQUEST_START && addr <= Z80_BUS_REQUEST_END)
	{
		fprintf(stderr, "Z80 BUS REQUEST READ: mem $%x\n", addr);
	}
	else if (addr >= Z80_RESET_START && addr <= Z80_RESET_END) 
	{
		// TODO: can you even read the z80 reset?
		fprintf(stderr, "Z80 RESET READ: mem $%x\n", addr);
	}
	else if (addr >= VERSION_REGISTER_START && addr <= VERSION_REGISTER_END)
	{
		fprintf(stderr, "VERSION REGISTER READ: mem $%x\n", addr);
	}
	else
	{
		fprintf(stderr, "UNIMPLEMENTED MEMORY READ: mem $%x\n", addr);
		exit(1);
	}
	
	return static_cast<unsigned int>(byte);
}

unsigned int m68k_read_memory_16(unsigned int addr)
{
	// TODO
	// if((addr & 0xe700e0) == 0xc00000)
	//	return ReadVDPW(addr&0x1e);

	unsigned int high_byte = m68k_read_memory_8(addr);
	unsigned int low_byte = m68k_read_memory_8(addr+1);

	unsigned int word = (high_byte << 8) | low_byte;

	return word;
}

unsigned int m68k_read_memory_32(unsigned int addr)
{
	unsigned int high_word = m68k_read_memory_16(addr);
	unsigned int low_word = m68k_read_memory_16(addr+2);

	unsigned int long_word = (high_word << 16) | low_word;

	return long_word;
}

void m68k_write_memory_8(unsigned int addr, unsigned int val)
{
	addr &= 0xFFFFFF;

	uint8_t val_b = static_cast<uint8_t>(val);

	if (addr >= ROM_START && addr <= ROM_END)
	{
		; // ROM is read-only
	}
	else if (addr >= RAM_START && addr <= RAM_END)
	{
		mmap.ram[addr-0xE00000] = val_b;
	}
	else if (addr >= Z80_MEMORY_START && addr <= Z80_MEMORY_END)
	{
		fprintf(stderr, "Z80 MEMORY WRITE: val $%x to mem $%x\n", val_b, addr);
	}
	else if ((addr >= VDP_DATA_PORT_START && addr <= VDP_DATA_PORT_END)
		  || (addr >= VDP_DATA_PORT_MIRROR_START && addr <= VDP_DATA_PORT_MIRROR_END))
	{
		fprintf(stderr, "VDP DATA PORT WRITE: val $%x to mem $%x\n", val_b, addr);
	}
	else if ((addr >= VDP_CONTROL_PORT_START && addr <= VDP_CONTROL_PORT_END)
		  || (addr >= VDP_CONTROL_PORT_MIRROR_START && addr <= VDP_CONTROL_PORT_MIRROR_END))
	{
		fprintf(stderr, "VDP CONTROL PORT WRITE: val $%x to mem $%x\n", val_b, addr);
	}
	else if (addr >= Z80_BUS_REQUEST_START && addr <= Z80_BUS_REQUEST_END)
	{
		fprintf(stderr, "Z80 BUS REQUEST WRITE: val $%x to mem $%x\n", val_b, addr);
	}
	else if (addr >= Z80_RESET_START && addr <= Z80_RESET_END)
	{
		fprintf(stderr, "Z80 RESET WRITE: val $%x to mem $%x\n", val_b, addr);
	}
	else if (addr >= VERSION_REGISTER_START && addr <= VERSION_REGISTER_END)
	{
		fprintf(stderr, "VERSION REGISTER WRITE: val $%x to mem $%x\n", val_b, addr);
	}
	else
	{
		fprintf(stderr, "UNIMPLEMENTED MEMORY WRITE: $%x to $%x\n", val_b, addr);
		exit(1);
	}
}

void m68k_write_memory_16(unsigned int addr, unsigned int val)
{
	unsigned int high_byte = (val & 0xFF00) >> 8;
	unsigned int low_byte = val & 0xFF;

	m68k_write_memory_8(addr, high_byte);
	m68k_write_memory_8(addr+1, low_byte);
}

void m68k_write_memory_32(unsigned int addr, unsigned int val)
{
	unsigned int high_word = (val & 0xFFFF0000) >> 16;
	unsigned int low_word = val & 0xFFFF;

	m68k_write_memory_16(addr, high_word);
	m68k_write_memory_16(addr+2, low_word);
}

unsigned int m68k_read_disassembler_8(unsigned int addr) {
	return m68k_read_memory_8(addr);
}

unsigned int m68k_read_disassembler_16 (unsigned int addr) {
	return m68k_read_memory_16(addr);
}

unsigned int m68k_read_disassembler_32 (unsigned int addr) {
	return m68k_read_memory_32(addr);
}

### IO Sample Program:
PASS

### FM Test:
NA - we dont have sound setup

### Graphics and Joystick Sampler:
PASS

### 68000 Opcode Sizes:
PASS

But, I had to build without support for detecting array out of bounds check as it tries to set vdp register 31, even though there are only 24 of these registers.

### 68000 Illegal Opcodes:
PASS

### V Counter Test Program:
loops forever waiting to read -1 from the HV counter:
```
(pc $0002de) cmpi.b  #-$1, D0                    (opc $0c0000ff)
(pc $0002e2) bne     $2dc                        (opc $66f8)
(pc $0002dc) move.b  (A2), D0                    (opc $1012)
VDP HV COUNTER READ 8: mem $c00008
(pc $0002de) cmpi.b  #-$1, D0                    (opc $0c0000ff)
(pc $0002e2) bne     $2dc                        (opc $66f8)
(pc $0002dc) move.b  (A2), D0                    (opc $1012)
VDP HV COUNTER READ 8: mem $c00008
...
```

### CRAM Flicker Test:
FAIL

stuck on all black screen looping forever writing to the vdp data port:
```
(pc $05d9a8) move.w  #$e, $c00000.l              (opc $33fc000e00c00000)
VDP DATA PORT WRITE 16: val $e to mem $c00000
(pc $05d9b0) move.w  #$e0, $c00000.l             (opc $33fc00e000c00000)
VDP DATA PORT WRITE 16: val $e0 to mem $c00000
(pc $0ca990) move.w  #$e00, $c00000.l            (opc $33fc0e0000c00000)
VDP DATA PORT WRITE 16: val $e00 to mem $c00000
```

### Sprite Masking Test:
FAIL

All test pass except for the one titled "Mask S1 on Dot Overflow".

### VDP Test Register:
FAIL

Aborts on vdprender.cpp line 125 due to 8-pixel rows not yet being implemented

### 68000 Memory Test:
FAIL

stuck in infinite loop trying to read the HV counter:
```
(pc $002356) cmpi.b  #-$1, D0                    (opc $0c0000ff)
(pc $00235a) bne     $2354                       (opc $66f8)
(pc $002354) move.b  (A2), D0                    (opc $1012)
VDP HV COUNTER READ 8: mem $c00008
(pc $002356) cmpi.b  #-$1, D0                    (opc $0c0000ff)
(pc $00235a) bne     $2354                       (opc $66f8)
(pc $002354) move.b  (A2), D0                    (opc $1012)
VDP HV COUNTER READ 8: mem $c00008
```

### 1536 Color Test:
FAIL

stuck in infinite loop trying to read the HV counter:
```
(pc $000274) cmpi.b  #-$1, D0                    (opc $0c0000ff)
(pc $000278) bne     $272                        (opc $66f8)
(pc $000272) move.b  (A2), D0                    (opc $1012)
VDP HV COUNTER READ 8: mem $c00008
(pc $000274) cmpi.b  #-$1, D0                    (opc $0c0000ff)
(pc $000278) bne     $272                        (opc $66f8)
(pc $000272) move.b  (A2), D0                    (opc $1012)
VDP HV COUNTER READ 8: mem $c00008
...
```

### Shadow Highlight Test:
FAIL

The test runs fine, but it shows that shadows are not implemented.

### Window Test:
PASS

scrolling looks good!

### Direct Color DMA
FAIL

stuck in infinite loop trying to read the lower byte of the VDP control port (0xC00005)

### Window Distortion Bug:
FAIL

stuck in infinite loop trying to read the HV counter:
```
(pc $000274) cmpi.b  #-$1, D0                    (opc $0c0000ff)
(pc $000278) bne     $272                        (opc $66f8)
(pc $000272) move.b  (A2), D0                    (opc $1012)
VDP HV COUNTER READ 8: mem $c00008
...
```

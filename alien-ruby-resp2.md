Adding the `--enable-load-relative` configure flag worked, and now our Ruby can
find it's built in libraries at test time!

I tested and can confirm that Ruby versions 2.1 through 3.1 can be built with the
`--enable-load-relative` flag on FreeBSD 13.1, 12.4, and 12.3 (all the
currently supported releases). However, for some reason none of these FreeBSD
versions can build Ruby 3.2, but this is not related to Alien::Ruby as I
reproduced the error outside of the Alien ecosystem without using the
`--enable-load-relative` flag. I opened a [bug report](https://bugs.freebsd.org/bugzilla/show_bug.cgi?id=269803) for this on the FreeBSD bug tracker.

Can this issue be closed now, as we can now test that Ruby can load and use a built-in library?

In regards to steps moving forward ...

### THIS IS NOT TRUE YET
I opened an MR (MR LINK) that allows the user to specify which Ruby version they want via the
`ALIEN_RUBY_VERSION` environment variable. I do not believe that it should be merged until after
I upgrade the CI to test against all the Ruby versions we support.
### END OF NOT TRUE YET

I agree that we should decide on a minimum Ruby version we want to support. On Windows,
the minimum version that RubyInstaller supports (with devkit) is 2.4.4.
All the RubyInstaller versions can be found in their archive [here](https://rubyinstaller.org/downloads/archives/). Is it ok for Windows to have a seperate minimum version than every other system? If so, for non-windows systems
2.1.0 may be a good minimum, as this is when Ruby changed their version scheme to a semantic-like
scheme. If we want to support versions from before Ruby settled on this version-scheme, then some additional logic will need to be added to the alienfile in MR (MR LINK) to support the old version scheme.

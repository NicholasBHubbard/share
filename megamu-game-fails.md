# Crashes/Freezes

##### NBA Live 98 (USA)
Crashes. Reproducable from start screen by choosing "exhibition mode", hit "start" a few times to skip ahead to start a game. Crashes with the game displaying the message "SEGA Library Internal Error. illegal instruction error at $a10002".

# FM-related Failures

##### Stormlord (USA)
Frozen at boot. Z80 stuck trying to read FM (0x4000)

##### Rampart (USA)
Frozen at boot. Z80 stuck trying to read FM (0x4000)

##### Thunder Fox (USA)
Freezes when hitting "start" in-game. It all starts with a bad read from FM (0x4000)

# Other

##### Elemental Master (USA)
The game works fine, but it is producing the output
```
UNEXPECTED CALL TO Z80_INPUT_BYTE. Port = bf. x = 1
```
I thought that this wasn't possible ... and this is the only game I have seen do it.

##### Super Street Fighter II
frozen at boot with all red screen.

SOLVED: this game is memory mapped

##### NBA Jam Tournament Edition
Frozen at boot with blue screen. Fails the same way in mame.

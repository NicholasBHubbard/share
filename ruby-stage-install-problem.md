# My Question

Is there anyway I can ignore the error I get from running Ruby 2.x.x from a staged
install, so the "hello" output is still printed to STDOUT?

# The Problem

On all Ruby 2.x.x versions, after compiling from source, I cannot run a simple
"hello" program from a staged installation. Strangely though, I am able to run
the program from Ruby 3.x.x and Ruby 1.8 and 1.9

The command I want to run from my staged install is simply `ruby -e 'puts "hello"'`.

The error I get when I run this on Ruby 2.x.x versions is this:

```
<internal:gem_prelude>:1:in `require': cannot load such file -- rubygems.rb (LoadError)
        from <internal:gem_prelude>:1:in `<compiled>'
```

On Ruby 3.x.x versions, the command does run successfully, but I get warnings
that seem very much related to the error on 2.x.x. Here is the output I get:

```
`RubyGems' were not loaded.
`did_you_mean' was not loaded.
hello
```

Finally, on Ruby 1.8 and 1.9, I just get the expected "hello" output with no
warnings.

I have confirmed that I get the same error on all Ruby 2.x.x versions.

# Steps to reproduce (2.7.7)

```
$ cd $HOME
$ mkdir -p tmp/realdest
$ mkdir tmp/stagedest
$ wget https://cache.ruby-lang.org/pub/ruby/2.7/ruby-2.7.7.tar.gz
$ tar -xf ruby-2.7.7.tar.gz
$ cd ruby-2.7.7
$ ./configure --prefix=$HOME/tmp/realdest
$ make
$ make DESTDIR=$HOME/tmp/stagedest install
$ $HOME/tmp/stagedest/$(whoami)/tmp/realdest/bin/ruby -e 'puts "hello"'
Traceback (most recent call last):
        1: from <internal:gem_prelude>:1:in `<internal:gem_prelude>'
<internal:gem_prelude>:1:in `require': cannot load such file -- rubygems.rb (LoadError)
```

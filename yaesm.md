yaesm:

After talking with Sriram we have came up with the idea for "yaesm", a snapshot manager and backup system for multiple filesystems (EXT4, and btrfs at least) based off of the yabsm btrfs snapshot manager. I am not going to go into too great of detail on yabsm here as there is already plenty of documentation for it available on its github page: https://github.com/NicholasBHubbard/Yabsm

Here are some important notes:

* Yabsm is designed to run as a dameon. Yabsm works using its own internal cron scheduler (with [Schedule::Cron](https://metacpan.org/pod/Schedule::Cron)). It seems that python has a similar package called [schedule](https://github.com/dbader/schedule). One of the key mistakes I made with yabsm was by providing the [daemon](https://metacpan.org/release/NHUBBARD/App-Yabsm-3.15.3/view/bin/yabsm#The-Yabsm-Daemon) subcommand. Our program should not daemonize itself! That is the job of the init system, and some init systems (such as runit) will not even support a daemon that forks itself.

* Testing: putting together a proper test suite will perhaps be the most difficult problem. We will need to setup mock enviornments with different filesystems, and server-client architectures (to test the remote backing up). There will be a ton of IO to control in our test suite. This is an area that yabsm could have done a lot better in, and because of this failure it makes it hard to safely make changes in yabsm. It is very important that we have good tests because a backup system is a critical system component that should never fail.

* Packaging: When we are finished we will need to package yaesm for all the OS's we want to support. We want our users to be able to install yaesm with their package manager. Note that we will likely support OS's other than Linux (FreeBSD for example supports ZFS).

* Configuration: Yabsm uses a custom configuration syntax. I think the syntax is nice and easy to understand, however I had to write an entire parser for it. If we used a standard syntax (such as INI) then we could just use a python library for parsing. The configuration syntax is a very important consideration because we want something easy for users to understand while also being very flexible. A nice thing about yabsm's parser however is that it gives really nice error reporting. Nice error reporting should in general be a goal of yaesm.

* Server-side configuration: For remote backups to work in yabsm the server being backed up to had to be configured. Yabsm solved this problem by just documenting what needed to be done server-side then providing functionality to check that the server was properly configured. See the manual on backing up with ssh: https://metacpan.org/release/NHUBBARD/App-Yabsm-3.15.3/view/bin/yabsm#SSH-Backups.

* In general we should try to do things simlarly to yabsm. It does a lot of things well. We can use yabsm as a template, and translate its code when necessary.

* We could provide an [rsync](https://rsync.samba.org/) backend that can be used to support users regardless of their filesystem.
